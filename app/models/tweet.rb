class Tweet < ApplicationRecord
  belongs_to :user

  validate :body_length
  validate :duplicate_tweet

  private

  def body_length
    errors.add(:body, 'size exceeded 180 characters') if body.size > 180
  end

  def duplicate_tweet
    errors.add(:body, 'same tweet sent in 24h') if Tweet.where('created_at > ?', 1.day.ago).where(user_id: user_id, body: body).any?
  end
end
