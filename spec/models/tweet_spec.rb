require 'rails_helper'

RSpec.describe Tweet, type: :model do
  describe 'validations' do
    let(:tweet) { FactoryBot.build(:tweet, body: body) }
    let(:subject) { tweet.valid? }

    describe 'body length validation' do
      describe 'with valid body' do
        let(:body) { 'some valid body'}

        it 'returns a valid record' do
          expect(subject).to eq(true)
          expect(tweet.errors.full_messages).to match_array([])
        end
      end
    end

    context 'with invalid body' do
      let(:body) { 'something' * 30 }

      it 'returns a valid record' do
        expect(subject).to eq(false)
        expect(tweet.errors.full_messages).to match_array(['Body size exceeded 180 characters'])
      end 
    end
  end
end
